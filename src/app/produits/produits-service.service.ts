import { Injectable } from '@angular/core';
import { Produit } from './produit.model';

@Injectable({
  providedIn: 'root'
})
export class ProduitsServiceService {

  public produits: Produit[] = [
    {
      id : '1',
      nom: 'Iphone X',
      prix: 600, 
      imageUrl:'https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/refurb-iphoneX-spacegray_AV2?wid=1144&hei=1144&fmt=jpeg&qlt=80&op_usm=0.5,0.5&.v=1548459944179',
      composants: ['Composant 1','Composant 2','Composant 3']
    },
    {
      id : '2',
      nom: 'Samsung Galaxy',
      prix: 550,
      imageUrl: 'https://www.samsungshop.tn/14324-large_default/samsung-a80-prix-tunisie.jpg',
      composants: ['Composant 1','Composant 2','Composant 3']
    },
  ];

  

  constructor() { }

  getAllProduits() {
    return [... this.produits ];
  }

  getProduit(produitId: string){
    return {
      ...this.produits.find(p =>{
        return p.id === produitId;
      })
    };
  }

  setProduit(produit:Produit){

    this.produits.push(produit);   
  }

  deleteProduit(produitId: string) {
    this.produits = this.produits.filter(p =>{
      return p.id !== produitId;
    })
  }

}
