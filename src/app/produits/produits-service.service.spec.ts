import { TestBed } from '@angular/core/testing';

import { ProduitsServiceService } from './produits-service.service';

describe('ProduitsServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProduitsServiceService = TestBed.get(ProduitsServiceService);
    expect(service).toBeTruthy();
  });
});
