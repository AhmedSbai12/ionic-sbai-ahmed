import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
 
  {
    path: '',
    redirectTo: 'produits',
    pathMatch: 'full'
  },
  {
    path: 'produits',
    children : [
      {
        path:'',
        loadChildren: () => import('./produits/produits.module').then( m => m.ProduitsPageModule)

      },
      {
        path: ':produitId',
        loadChildren: () => import('./produits/produit-details/produit-details.module').then( m => m.ProduitDetailsPageModule)
      },

    ]
  },
  {
    path: 'add',
    loadChildren: ()=> import('./add-produit/add-produit.module').then(m => m.AddProduitPageModule)
  }
   
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
