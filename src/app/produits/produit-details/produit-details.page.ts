import { Component, OnInit } from '@angular/core';
import { Produit } from '../produit.model';
import { ActivatedRoute, Router } from '@angular/router';
import { ProduitsServiceService } from '../produits-service.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-produit-details',
  templateUrl: './produit-details.page.html',
  styleUrls: ['./produit-details.page.scss'],
})
export class ProduitDetailsPage implements OnInit {
  loadedProduit: Produit;
  
  
  constructor(
    private activatedRoute: ActivatedRoute,
    private produitService:ProduitsServiceService,
    private router: Router,
    private alertCtrl: AlertController
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(paramMap =>{
      if(!paramMap.has('produitId')){
        //redirect
        this.router.navigate(['/produits']);
        return;
      }
      const produitId = paramMap.get('produitId');
      this.loadedProduit = this.produitService.getProduit(produitId);
    });
  }

  onDeleteProduit(){
    this.alertCtrl.create({header: 'Are you sure?',
    message:'Do you really want to delete this recipe',
    buttons: [{
      text: 'Cancel',
      role: 'cancel'
    },
    {
      text: 'Delete',
      handler:() => {
        this.produitService.deleteProduit(this.loadedProduit.id);
        this.router.navigate(['/produits']);
      }
    }]
  }).then(alertEl =>{
    alertEl.present();
  })
  }

}
