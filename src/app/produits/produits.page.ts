import { Component, OnInit } from '@angular/core';
import { Produit } from './produit.model';
import { ProduitsServiceService } from './produits-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-produits',
  templateUrl: './produits.page.html',
  styleUrls: ['./produits.page.scss'],
})
export class ProduitsPage implements OnInit {
  public produits: Produit[];
  
  constructor(
    private produitService: ProduitsServiceService,
    private router: Router
  ) { }

  ngOnInit() {
    this.produits= this.produitService.getAllProduits();
  }

  onAddProduitButton(){
      this.router.navigate(['/add']);
  }

}
