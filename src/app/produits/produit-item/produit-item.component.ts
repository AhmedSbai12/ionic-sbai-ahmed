import { Component, OnInit, Input } from '@angular/core';
import { Produit } from '../produit.model';

@Component({
  selector: 'app-produit-item',
  templateUrl: './produit-item.component.html',
  styleUrls: ['./produit-item.component.scss'],
})
export class ProduitItemComponent implements OnInit {
  @Input() public produitItem: Produit;

  constructor() { }

  ngOnInit() {}

}
